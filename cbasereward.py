import sys
import os
from bitcoinrpc.authproxy import AuthServiceProxy

credfile = os.environ['HOME'] + "/.bitcoin/bitcoin.conf"
creds = dict()

with open(credfile, "r") as crd:
    lines = crd.readlines()
    for line in lines:
        tmplst = line.split('=')
        if len(tmplst) >= 2:
            creds[tmplst[0].strip()] = '='.join(tmplst[1:]).strip()

if len(sys.argv) > 1:
    startblock = int(sys.argv[1])
else:
    startblock = 1

rconn = AuthServiceProxy("http://%s:%s@127.0.0.1:8332"%(creds['rpcuser'], creds['rpcpassword']))

# 100 blocks at a time
tbcnt = int(rconn.getblockcount())

bbatch = list()
while (tbcnt - startblock) > 100:
    bbatch.append(list(range(startblock, startblock+100)))
    startblock += 100
bbatch.append(list(range(startblock, tbcnt)))

for btch in bbatch:
    cmdlst = [["getblockhash", height] for height in btch]
    bhashes = rconn.batch_(cmdlst)
    bheads = rconn.batch_([["getblock", hsh] for hsh in bhashes])
    outlist = [[str(hd['height']), str(hd['mediantime']), str(hd['nTx']), str(hd['size']), str(hd['difficulty'])] for hd in bheads]
    rtxlst = [["getrawtransaction", hd2['tx'][0]] for hd2 in bheads]
    rtx = rconn.batch_(rtxlst)
    cbtx = rconn.batch_([["decoderawtransaction", tx] for tx in rtx])
    # Add all values, should be only one
    cbaseval = list()
    for tx in cbtx:
        if len(tx['vin']) == 1 and 'coinbase' in tx['vin'][0]:
            total = 0
            for vo in tx['vout']:
                total += vo['value']
            cbaseval.append(total)
        else:
            print("Transaction " + tx['txid'] +" is not a coinbase tx")
            sys.exit(1)
    for rslt in range(len(btch)):
        print('\t'.join(outlist[rslt]) + '\t' + str(cbaseval[rslt]))

