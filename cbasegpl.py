#!/usr/bin/env python3

# Retrieve the data from the database and generate input data for gnuplot

import sqlite3
import sys
from datetime import datetime

# Hashrate for difficulty 1 (Genesis block) in MH/s
gendiff = 2**48/65535/6/10**8

#Read database into a list
# date, blocks, ntx, size, difficulty, height ,reward, fee, price
#  0      1      2     3       4         5       6      7     8
conn = sqlite3.connect('coinbase.sqlite')
cur = conn.cursor()
cur.execute('select date, height from coinbase where date = ' + \
        ' (select max(date) from coinbase)')
lastrow = cur.fetchone()
lastblock = int(lastrow[1])
if lastblock >= 630000:
    cur.execute('select min(date) from coinbase where height >= 630000')
    half3 = str(int(cur.fetchone()[0]) * 86400)
else:
    half3 = str(int(lastrow[0])*86400 + (630000 - lastblock)*600)

lastdate = datetime.fromtimestamp(int(lastrow[0])*86400).strftime('%F')
cur.execute('select * from coinbase order by date')
blkdata = cur.fetchall()
if len(blkdata) == 0:
    sys.stderr.write("Error getting data from database\n")
    sys.exit(1)

# coinbase & fee price, hash rate
cbdata = open("cbdata.csv", "w")
for rec in blkdata:
    dt = rec[0] * 86400
    cbprice = rec[6] * rec[8] / rec[1]
    feeprice = rec[7] * rec[8] / rec[1]
    feetx = rec[7] * rec[8] / rec[2]
    hashrate = gendiff * rec[4] * rec[1] / 144
    cbdata.write(",".join(str(x) for x in [dt, cbprice, feeprice, feetx, \
            hashrate, rec[8]])+"\n")
cbdata.close()

gpscript = open("cbchart.gpl", "w")
gpscript.write("""#!/usr/bin/gnuplot
# Gnuplot script to generate a chart of the coinbase rewards and fee

set terminal svg size 1600,960
set termopt enhanced
set output "coinbaseprice.svg"

HALF1=86400*15673
HALF2=86400*16992
""")
gpscript.write("HALF3=" + half3)
gpscript.write("\nCTRH3=" + str((86400*16992+int(half3))/2))
gpscript.write("\nMAXDATE=\"" + lastdate + '"')
gpscript.write("""
BANN1="Chart by Rob Wolfram (Twitter \\\\@hamal03)\\n"
BANN2="Data retrieved from coinmetrics.io and full node\\n" 
BANN3="Data until ".MAXDATE
BANNER=BANN1.BANN2.BANN3

set timefmt "%s"
set xdata time
set format x "%Y"
# print background
set obj 1 rect from graph 0, graph 0 to HALF1, graph 1 fs solid 0.7 \\
    fillcolor '0xeeeeff' behind
set obj 2 rect from HALF1, graph 0 to HALF2, graph 1 fs solid 0.7 \\
    fillcolor '0xffeeee' behind
set obj 3 rect from HALF2, graph 0 to HALF3, graph 1 fs solid 0.7 \\
    fillcolor '0xeeffee' behind
set obj 4 rect from HALF3, graph 0 to graph 1, graph 1 fs solid 0.7 \\
    fillcolor '0xffffee' behind
# print halving lines
set arrow from HALF1, graph 0 to HALF1, graph 1 nohead lt 2 lc '0xcccccc' lw 2
set arrow from HALF2, graph 0 to HALF2, graph 1 nohead lt 2 lc '0xcccccc' lw 2
set arrow from HALF3, graph 0 to HALF3, graph 1 nohead lt 2 lc '0xcccccc' lw 2

set title "Average value of Bitcoin coinbase transactions" font "Helvetica Bold,30"
set ylabel "Coinbase tx / fee value (USD)" font "Helvetica,16"
set y2label "Hash rate (Megahash/s)" font "Helvetica,16"
set key box opaque height 1 left font "Helvetica,14"
set obj 5 rect at CTRH3, 0.0005 size char strlen(BANN2), char 5 fs solid 1 front
set label 1 at CTRH3, 0.0008 BANNER front center font "Helvetica,14"
set datafile separator ","
set logscale y
set logscale y2
set grid x y y2
set xtics
set ytics
set nomytics
set y2tics
set nomy2tics
set xrange [1270080000:]
set yrange [0.0001:]
set key box opaque height 3 left

plot \\
    "cbdata.csv" using 1:6 axes x1y1 with lines lw 2 lc '0xbbbbbb' notitle, \\
    "cbdata.csv" using 1:2 axes x1y1 with lines lw 2 lc 'blue' t "Avg. coinbase value / block", \\
    "cbdata.csv" using 1:3 axes x1y1 with lines lw 2 lc 'purple' t "Avg. fee value / block", \\
    "cbdata.csv" using 1:4 axes x1y1 with lines lw 2 lc '0x00c000' t "Avg. fee value / tx", \\
    "cbdata.csv" using 1:5 axes x1y2 with lines lw 2 lc 'orange' t "Appr. hash rate"
""")
