#!/usr/bin/env python3

# Fill a SQLite database with coinbase amounts, block sizes and
# price information on a granularity of a day.

import sqlite3
import sys
import requests
import json
import fileinput
from datetime import datetime
from math import ceil

# Get last date
conn = sqlite3.connect('coinbase.sqlite')
cur = conn.cursor()
cur.execute('select max(date) from coinbase')
lastdate = cur.fetchone()[0]
if lastdate is None:
    lastdate = 14244
else:
    lastdate = int(lastdate)

# Get price data
cmprise = dict()
burl = "https://community-api.coinmetrics.io/v2/assets/btc/metricdata"
bapistr = '?metrics=PriceUSD&start='
tdago = datetime.fromtimestamp(lastdate*86400).strftime('%F')
newdata = requests.get(burl+bapistr+tdago)
if newdata.status_code != 200:
    print("Getting data from coinmetrics failed")
    sys.exit(1)
jdata = json.loads(newdata.text)
for bd in jdata['metricData']['series']:
    if bd['values'][0] is None: break
    epdate = int(int(datetime.strptime(bd['time'], '%Y-%m-%dT%H:%M:%S.000Z').\
	strftime('%s'))/86400+.5)
    cmprise[epdate] = bd['values'][0]
if len(cmprise) == 0:
    sys.stderr.write("No new data available at Coinmetrics\n")
    sys.exit(0)

# Read blockchain data from stdin
today = int(int(datetime.now().strftime('%s'))/86400)
bcdict = dict()
for line in fileinput.input():
    bcline = line.rstrip().split('\t')
    # height mediantime nTx size difficulty coinbase
    bcdate = ceil(int(bcline[1])/86400)
    if bcdate not in cmprise or bcdate == today: continue
    hgtpwr = int(int(bcline[0])/210000)
    defbase = 50.0/2**hgtpwr
    reward = float(bcline[5])
    if bcdate not in bcdict:
        bcdict[bcdate] = dict()
        bcdict[bcdate]['blocks'] = 0
        bcdict[bcdate]['ntx'] = 0
        bcdict[bcdate]['size'] = 0
        bcdict[bcdate]['reward'] = 0
        bcdict[bcdate]['fee'] = 0

    bcdict[bcdate]['blocks'] += 1
    bcdict[bcdate]['ntx'] += int(bcline[2])
    bcdict[bcdate]['size'] += int(bcline[3])
    bcdict[bcdate]['height'] = int(bcline[0])
    bcdict[bcdate]['difficulty'] = float(bcline[4])
    bcdict[bcdate]['reward'] += reward
    if reward > defbase:
        bcdict[bcdate]['fee'] += (reward - defbase)

if len(bcdict) == 0:
    sys.stderr.write("No new data in blockchain input\n")
    sys.exit(1)

# CREATE TABLE coinbase ( date int PRIMARY KEY, blocks int, ntx int, size int,
#     difficulty float, height int, reward float, fee float, price float );
sqlins = "INSERT OR IGNORE INTO coinbase VALUES (?,?,?,?,?,?,?,?,?)"

cur.execute('select max(height) from coinbase')
dbblock = cur.fetchone()[0]
if dbblock is None:
    dbblock = 0
for mydate in range(lastdate, min(max(bcdict.keys()), max(cmprise.keys())) + 1):
    if mydate not in bcdict: continue
    cur.execute(sqlins, [mydate, bcdict[mydate]['blocks'], bcdict[mydate]['ntx'],\
        bcdict[mydate]['size'], bcdict[mydate]['difficulty'], bcdict[mydate]['height'],\
        bcdict[mydate]['reward'], bcdict[mydate]['fee'], cmprise[mydate]])

conn.commit()

