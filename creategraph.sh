#!/bin/bash

export PATH="/bin:/usr/bin"
export NODE="btcfull.hamal.nl"

cd $(dirname $0)
DBMOD=$(stat -c "%Y" coinbase.sqlite 2>/dev/null)
if [ $? -ne 0 ]; then
    echo 'coinbase.sqlite not found' >&2
    exit 1
fi

export height=$(sqlite3 coinbase.sqlite "select max(height) from coinbase") 2>/dev/null
if [ $? -ne 0 ] ; then
    echo "SQLite database 'coinbase.sqlite' is incorrect" >&2
    exit 1
fi
[ -z "$height" ] && height=1

ssh $NODE "bin/cbasereward.py $height" | python3 cbasedbfill.py 2>/dev/null
DBNEW=$(stat -c "%Y" coinbase.sqlite)
[ "${DBMOD}" -eq "${DBNEW}" ] && exit 0
python3 cbasegpl.py
gnuplot cbchart.gpl
convert coinbaseprice.svg coinbaseprice.png
sftp -q s2f@s2f.hamal.nl:html/images/. <<< $'put coinbaseprice.png' >/dev/null 2>&1
