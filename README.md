# Bitcoin Coinbase value

The "Halvening" is an essential part of Bitcoin's protocol. After every
210,000 blocks (i.e. approximately 4 years) the reward for the miner that
finds correct block is cut in half. At the time of writing this, it happened
two times before and and is on the verge of happening again. I wondered what
effect the halvening had on the earning of the miners and if it has any effect
of miners capitulating due to a steep loss of earnings.

I created a few scripts that will retrieve data from the blockchain and 
[coinmetrics.io](https://coinmetrics.io/community-network-data/) and aggregate
the data on a daily basis which I use to create a graph. The graph shows the
daily average of the dollar values of:
* the total coinbase transaction (i.e, new coins and fee)
* the fee component of the coinbase transaction
* the fee per transaction

In addition to that the approximate hash rate is drawn, based on the
difficulty of the day's last block and the number of mined blocks of the day.

The graph is used on [this blog page](https://s2f.hamal.nl/coinbase.html) and
is updated daily. Interpreting the graph is left to the reader.

## Method

I use three python scripts to create an gnuplot script and accompanying
input file to create the graph. 

* *cbasereward.py:*<br/>This runs on a full node and retrieves the necessary
information from the blockchain and sends the output to stdout. The script can
be called with a block height as argument and it will then only provide the
blockchain data starting from that block height.
* *cbasedbfill.py:*<br/>This uses the output from the previous script and
price data from coinmetrics.io, aggregated the data with a daily granularity 
and adds the data to a SQLite database.
* *cbasegpl.py:*<br/>This reads the data from the SQLite database and
calculates values to be placed in an input file for gnuplot. The script also
generated the gnuplot script itself.

The SQLite database contains a single table with the following schema:
<pre>
CREATE TABLE coinbase ( date int PRIMARY KEY, blocks int, ntx int, size int,
    difficulty float, height int, reward float, fee float, price float );
</pre>
The script expects the name of the database to be "coinbase.sqlite".
The date field is the Unix epoch divided by 86400.

The scripts generate 2 files:
* cbchart.gpl: this is the gnuplot script to generate the graph image. This is
not static because the next halvening date is approximated and that value is
included in the script, as is the date to which the graph is generated.
* cbdata.csv: this is a comma separated file with the input values to the 
gnuplot script.

The gnuplot script produces an SVG image file which I convert with ImageMagick
to a PNG file. IMHO this yields a better result than generating a PNG image
with gnuplot.

## Data handling

The time stamps of the blockchain that we use is the "mediantime" value. The
reason is that this value is guaranteed to increase with every block while the
"time" value doesn't. The "date" value that is generated from that is
calculated as "ceil(mediantime/86400)". The values of the "size", "ntx",
"tx[0]['vout'][0]['value']" and the calculated fee are added to a value which
is stored in a dict with the date as the key. The fee is calculated by
subtracting the value of the new coins from the total coinbase output. The
number of blocks per date and the last difficulty per date are similarly
stored, as is the US Dollar price, retrieved from coinmetrics.io.

For calculating the values for the graph, the coinbase value and fee value are
divided by the number of blocks in the day and multiplied by the price. For
the fee per transaction the fee value is divided by the number of transactions
instead.

The Bitcoin Core api returns a difficulty value i.o. the difficulty target.
This value is the difficulty target of the genesis block divided by that of
the examined block. I used that to approximate the hash rate by multiplying
the hash rate of the genesis block (a bit more than 7 megahashes/second) with
the difficulty value. This value is adjusted for the day by multiplying with
the number of blocks and dividing by 144.

## Requirements

All python scripts assume python3 and except for the first one named here,
they do only depend on basic modules.

The "cbasereward.py" script needs to run on a full node and expects a
".bitcoin/bitcoin.conf" file containing RPC username and password in the home
directory of the user running the script. The script requires the *bitcoinrpc*
module, which can be installed from PyPI (module name "python-bitcoinrpc").
The full node needs to be indexed (txindex=1 in bitcoin.conf).

I use gnuplot version 5.0 for rendering the graph. I can't guarantee a proper
rendering on older versions.

